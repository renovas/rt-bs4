<?php get_header(); ?>



<div id="content" class="site-content<?php if ( is_active_sidebar('sidebar-widget') ) { echo ' with-widget'; } ?>">
<div class="site-content-inner">



<div id="primary" class="content-area">
<main id="main" class="site-main">


<?php
if ( have_posts() ) :
  while ( have_posts() ) : the_post();
    get_template_part( 'template-parts/post/content', 'page' );
  endwhile;
else :
  get_template_part( 'template-parts/post/content', 'none' );
endif; ?>


</main><!-- / .site-main -->
</div><!-- / .content-area -->



<?php get_sidebar(); ?>



</div><!-- / .site-content-inner -->
</div><!-- / .site-content -->



<?php get_footer(); ?>

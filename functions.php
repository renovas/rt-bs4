<?php

/**
* よく使うスニペット類を読み込み
*/
require_once( get_template_directory().'/inc/snippets.php' );
require_once( get_template_directory().'/inc/shortcodes.php' );
require_once( get_template_directory().'/inc/load_scripts.php' );



/**
 * コンテンツ幅の定義
 * @link https://codex.wordpress.org/Content_Width
 */
if ( ! function_exists( 'set_content_width' ) ) :
function set_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'set_content_width', 640 );
}
add_action( 'after_setup_theme', 'set_content_width', 0 );
endif; // set_content_width



/**
 * カスタムロゴを有効化
 *
 * @link https://codex.wordpress.org/Theme_Logo
 */
if ( ! function_exists( 'enable_custom_logo' ) ) :
function enable_custom_logo() {
  add_theme_support( 'custom-logo', array(
  	'height'      => 250,
  	'width'       => 250,
  	'flex-width'  => true,
  	'flex-height' => true,
  ) );
}
add_action( 'after_setup_theme', 'enable_custom_logo' );
endif; // enable_custom_logo



// カスタム背景を有効化
if ( ! function_exists( 'enable_custom_background' ) ) :
function enable_custom_background() {
  add_theme_support( 'custom-background', apply_filters( 'enable_custom_background', array(
  	'default-color' => 'ffffff',
  	'default-image' => '',
  ) ) );
}
add_action( 'after_setup_theme', 'enable_custom_background' );
endif; // enable_custom_background



/**
 * カスタムヘッダを有効化
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 */
if ( ! function_exists( 'enable_custom_header' ) ) :
function enable_custom_header() {
	add_theme_support( 'custom-header', apply_filters( 'enable_custom_header', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
		'wp-head-callback'       => '',
	) ) );
}
add_action( 'after_setup_theme', 'enable_custom_header' );
endif; // enable_custom_header



/**
 * カスタムメニューの定義
 * @link https://codex.wordpress.org/Function_Reference/register_nav_menus
 */
if ( ! function_exists( 'my_init_custom_menu' ) ) :
function my_init_custom_menu() {
register_nav_menus( array(
	'global' => 'グローバルナビ',
	'header' => 'ヘッダナビ',
	'footer' => 'フッタナビ',
	'side' => 'サイドナビ',
) );
}
add_action( 'after_setup_theme', 'my_init_custom_menu' );
endif; // my_init_custom_menu



/**
 * ウィジェットの定義
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
if ( ! function_exists( 'my_init_widgets' ) ) :
function my_init_widgets() {
	// sidebar-widget
	register_sidebar( array(
		'name' => 'サイドバー',
		'id' => 'sidebar-widget',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	// sidebar-banner
	register_sidebar( array(
		'name' => 'サイドバナー',
		'id' => 'sidebar-banner',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="sidebar-banner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	// header-widget
	register_sidebar( array(
		'name' => 'ヘッダ',
		'id' => 'header-widget',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title' => '',
		'after_title' => '',
	) );

	// footer-widget
	register_sidebar( array(
		'name' => 'フッタ',
		'id' => 'footer-widget',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title' => '',
		'after_title' => '',
	) );

	// sidebar-home
	register_sidebar( array(
		'name' => 'HOME用サイドバー',
		'id' => 'sidebar-home',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title' => '',
		'after_title' => '',
	) );

	// ウィジェットカスタマイザー
	add_theme_support( 'widget-customizer' );

	// テキストウィジェットでショートコードを使用
	add_filter('widget_text', 'do_shortcode');
}
add_action( 'widgets_init', 'my_init_widgets' );
endif; // my_init_widgets



/**
 * サムネールサイズの設定(post-thumbnail)
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 * @link https://codex.wordpress.org/Option_Reference#Media
 */
if ( ! function_exists( 'custom_thumbnail_size' ) ) :
// サムネールの追加サイズ
function custom_thumbnail_size() {
	//add_image_size('news-thumbnail', 225, 152, false);
}
//add_action( 'after_setup_theme', 'custom_thumbnail_size' );
endif; // custom_thumbnail_size


// デフォルトサイズの定義(thumbnail/medium/large)
//set_post_thumbnail_size(150, 150, false);
if ( ! function_exists( 'thumbnail_size_setup' ) ) :
function thumbnail_size_setup() {
	if(update_option('thumbnail_size_w', 150)) { // default: 150
		update_option('thumbnail_size_h', 150);
	}
	if(update_option('medium_size_w', 300)) { // default: 300
		update_option('medium_size_h', 300);
	}
	if(update_option('large_size_w', 1024)) { // default: 1024
		update_option('large_size_h', 1024);
	}
}
//add_action('after_setup_theme', 'thumbnail_size_setup');
endif; // thumbnail_size_setup


// サムネールの自動トリミングON/OFF
if ( ! function_exists( 'thumbnail_crop_setup' ) ) :
function thumbnail_crop_setup() {
	if(false === get_option('thumbnail_crop')) {
		add_option('thumbnail_crop', '1'); }
	else {
		update_option('thumbnail_crop', '1');
	}
	if(false === get_option('medium_crop')) {
		add_option('medium_crop', '1'); }
	else {
		update_option('medium_crop', '1');
	}
	if(false === get_option('large_crop')) {
		add_option('large_crop', '1'); }
	else {
		update_option('large_crop', '1');
	}
}
//add_action('after_setup_theme', 'thumbnail_crop_setup');
endif; // thumbnail_crop_setup



/**
 * 一覧の取得条件(Query)のデフォルト設定を定義
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts
 */
if ( ! function_exists( 'set_post_per_page' ) ) :
function set_post_per_page( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
	return;

	// 投稿タイプのアーカイブページ
	if ( $query->is_post_type_archive( 'xxxx' ) ) {
		$query->set( 'posts_per_page', 10 );
		$query->set( 'nopaging', 0 );
		return;
	}

	// 投稿のアーカイブページ
	if ( $query->is_home() ) {
		$query->set( 'posts_per_page', 20 );
		$query->set( 'nopaging', 0 );
		return;
	}
}
//add_action( 'pre_get_posts', 'set_post_per_page');
endif; // set_post_per_page

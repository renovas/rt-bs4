<?php

/**
 * ショートコードの定義
 * @link https://codex.wordpress.org/Shortcode_API
 *
 * 関数一覧
 * ウィジェット [widget id="xxxx" before="xxxx" after="xxxx"]
 * カスタムニュー（wp_nav_menu） [nav_menu menu='xxx']
 * 子ページの本文 [load_children]
 * アイキャッチ画像 [thumbnail size="full" class="img-fluid"]
 * ホームのアドレス [home_url]
 * サイトのアドレス [site_url]
 * テンプレートディレクトリ [template_dir]
 * アップロードディレクトリ [upload_dir]
 * PCとMobileで表示コンテンツを変える [desktop]xxxx[/desktop] [mobile]xxxx[/mobile]
 * E-Mailアドレスをエンティティ化する [email]mail@exsample.com[/email]
 * 任意の投稿（本文） [post_content id="xxxx"]
 * 任意の投稿タイトル [post_title id="xxxx"]
 * 任意の投稿URL [post_permalink id="xxxx"]
 * 任意のカスタムフィールド [post_field key="xxxx" id="xxxx" type="xxxx"]
 * 任意のテンプレートファイル [fileinclude name='xxxx']
 * 投稿一覧をリスト表示 [postlist post_type='post' limit='-1' orderby='date' order='DESC' taxonomy='' term='' list_class='' item_class=''' date_format='Y/m/d' disp_cat='false']
 *
 */


// Shortcode: ウィジェット [widget id="xxxx" before="xxxx" after="xxxx"]
function get_widget_shortcode( $atts , $content = null ) {
	extract( shortcode_atts(array(
		'id' => null,
		'before' => null,
		'after' => null,
	), $atts ));
	$output = '';
	if ( is_active_sidebar($id) ) {
		ob_start();
		echo $before;
		dynamic_sidebar($id);
		echo $after;
		$output = ob_get_clean();
	}
	return $output;
}
add_shortcode( 'widget', 'get_widget_shortcode' );


// Shortcode: カスタムニュー（wp_nav_menu） [nav_menu menu='xxx']
function custom_menu_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'menu'            => '',
			'container'       => 'div',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'walker'          => '',
			'theme_location'  => '',
			'level'           => 0
		),
		$atts
	));
	return wp_nav_menu(
		array(
			'menu'            => $menu,
			'container'       => $container,
			'container_class' => $container_class,
			'container_id'    => $container_id,
			'menu_class'      => $menu_class,
			'menu_id'         => $menu_id,
			'echo'            => false,
			'fallback_cb'     => $fallback_cb,
			'before'          => $before,
			'after'           => $after,
			'link_before'     => $link_before,
			'link_after'      => $link_after,
			'depth'           => $depth,
			'walker'          => $walker,
			'theme_location'  => $theme_location,
			'level'           => $level
		)
	);
}
add_filter( 'wp_nav_menu_objects', 'submenu_limit', 10, 2 );

add_shortcode( "nav_menu", "custom_menu_shortcode" );

function submenu_limit( $items, $args ) {
	if ( empty($args->submenu) )
		return $items;
		$parent_id = array_pop( wp_filter_object_list( $items, array( 'title' => $args->submenu ), 'and', 'ID' ) );
		$children  = submenu_get_children_ids( $parent_id, $items );
		foreach ( $items as $key => $item ) {
		if ( ! in_array( $item->ID, $children ) )
			unset($items[$key]);
	}
	return $items;
}

function submenu_get_children_ids( $id, $items ) {
	$ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );
	foreach ( $ids as $id ) {
		$ids = array_merge( $ids, submenu_get_children_ids( $id, $items ) );
	}
	return $ids;
}


// Shortcode: 子ページの本文を読み込み [load_children]
function load_children_shortcode() {
	global $post;
	$retval = null;
	$args = array ( 'post_status' => 'publish', 'numberposts' => -1, 'orderby' => 'menu_order', 'order' => 'ASC', 'post_parent' => $post->ID );
	$children = get_children($args);
	foreach ( $children as $post ) {
		$retval .= apply_filters('the_content',$post->post_content);
	}
	return $retval;
}
add_shortcode('load_children', 'load_children_shortcode');


// Shortcode: アイキャッチ画像 [thumbnail size="full" class="img-fluid"]
function post_thumbnail_shortcode( $atts ) {
	extract( shortcode_atts(
		array(
			'size' => 'full',
			'class' => 'img-responsive center-block',
		), $atts )
	);
	if ( has_post_thumbnail() ) {
		return get_the_post_thumbnail( $post_id, $size, 'class='.$class );
	} else {
		return;
	}
}
add_shortcode( 'thumbnail', 'post_thumbnail_shortcode' );


// Shortcode: ホームのアドレス [home_url]
function home_address_shortcode() {
	return home_url();
}
add_shortcode('home_url', 'home_address_shortcode');


// Shortcode: サイトのアドレス [site_url]
function site_address_shortcode() {
	return site_url();
}
add_shortcode('site_url', 'site_address_shortcode');


// Shortcode: テンプレートディレクトリ [template_dir]
function template_dir_shortcode() {
	return get_stylesheet_directory_uri();
}
add_shortcode('template_dir', 'template_dir_shortcode');


// Shortcode: アップロードディレクトリ [upload_dir]
function upload_base_shortcode() {
	$upload_dir = wp_upload_dir();
	return $upload_dir['baseurl'];
}
add_shortcode('upload_dir', 'upload_base_shortcode');


// Shortcode: PCとMobileで表示コンテンツを変える [desktop]xxxx[/desktop] [mobile]xxxx[/mobile]
// [desktop]xxxx[/desktop] PC用
function my_desktop_only_shortcode( $atts, $content = null ) {
	if ( !wp_is_mobile() ) {
		return wpautop( do_shortcode( $content ) );
	} else {
		return null;
	}
}
add_shortcode('desktop', 'my_desktop_only_shortcode');

// [mobile]xxxx[/mobile] Mobile用
function my_mobile_only_shortcode( $atts, $content = null ) {
	if ( wp_is_mobile() ) {
		return  wpautop( do_shortcode( $content ) );
	} else {
		return null;
	}
}
add_shortcode('mobile', 'my_mobile_only_shortcode');


// E-Mailアドレスをエンティティ化する [email]mail@exsample.com[/email]
function antispambot_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
add_shortcode( 'email', 'antispambot_shortcode' );


// Shortcode: 任意の投稿（本文） [post_content id="xxxx"]
function get_post_content_shortcode( $atts , $content = null ) {
	extract( shortcode_atts(array('id' => get_the_ID(),), $atts ));
	$output = '';
	$post_obj = get_post($id);
	$output = apply_filters('the_content', $post_obj->post_content);
	return $output;
}
add_shortcode( 'post_content', 'get_post_content_shortcode' );


// Shortcode: 任意の投稿タイトル [post_title id="xxxx"]
function get_post_title_shortcode( $atts , $content = null ) {
	extract( shortcode_atts(array('id' => get_the_ID(),), $atts ));
	$output = '';
	$post_obj = get_post($id);
	$output = apply_filters('the_title', $post_obj->post_title);
	return $output;
}
add_shortcode( 'post_title', 'get_post_title_shortcode' );


// Shortcode: 任意の投稿URL [post_permalink id="xxxx"]
function get_post_permalink_shortcode( $atts , $content = null ) {
	extract( shortcode_atts(array('id' => get_the_ID(),), $atts ));
	$output = '';
	$post_obj = get_post($id);
	$output =  esc_url_raw($post_obj->guid);
	return $output;
}
add_shortcode( 'post_permalink', 'get_post_permalink_shortcode' );


// Shortcode: 任意のカスタムフィールド [post_field key="xxxx" id="xxxx" type="xxxx"]
function get_post_field_shortcode( $atts , $content = null ) {
	extract( shortcode_atts(array('key' => null, 'id' => get_the_ID(), 'type' => 'text',), $atts ));
	$output = '';
	$field = get_field( $key, $id );
	if ( 'number' == $type ) {
		$field = number_format($field);
	}
	$output = esc_html($field);
	return $output;
}
add_shortcode( 'post_field', 'get_post_field_shortcode' );


// Shortcode: 任意のテンプレートファイル [fileinclude name='xxxx']
function file_include_shortcode( $atts, $content = null ) {
	extract(shortcode_atts( array('name' => ''), $atts ));
	ob_start();
	//include( get_theme_root() . '/' . get_stylesheet() . '/' . $name ); /* case1 */
	locate_template( $name, true, true ); /* case2 */
	//get_template_part( $name ); /* case3 */
	$content = ob_get_clean();
	return do_shortcode($content);
}
add_shortcode('fileinclude', 'file_include_shortcode');


// Shortcode: 投稿一覧をリスト表示 [postlist post_type='post' limit='-1' orderby='date' order='DESC' taxonomy='' term='' list_class='' item_class=''' date_format='Y/m/d' disp_cat='false']
function postlist_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'limit'          => -1,
		'post_type'      => 'post',
		'taxonomy'       => null,
		'term'           => null,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'list_class'     => null,
		'item_class'     => null,
		'icon'           => 'fa fa-angle-right',
		'date_format'    => 'Y/m/d',
		'disp_cat'       => false,
	), $atts ) );
	query_posts( array (
		'post_status'    => 'publish',
		'posts_per_page' => $limit,
		'post_type'      => $post_type,
		'taxonomy'       => null,
		'term'           => null,
		'orderby'        => $orderby,
		'order'          => $order
	) );

	// vars
	$post_items = '';
	$cats = '';
	$item_class_var = !empty($item_class) ? ' class="'.$item_class.'"' : '';
	$list_class_var = !empty($list_class) ? ' class="'.$list_class.'"' : '';

	while ( have_posts() ) : the_post();
		// category
		if ( !empty($disp_cat) && ( empty($taxonomy) && !empty($term) ) ) {
			$cats = get_the_terms( $post->ID, $taxonomy );
			if ( isset($cats) ) {
				$cat = $cats[0];
				$cat_slug = $cat->slug;
				$cat_name = $cat->name;
			} else {
				$cat_slug = 'others';
				$cat_name = 'その他';
			}
		}

		// output
		$post_items .= '<li'.$item_class_var.' id="post-'. esc_attr(get_the_ID()) . '">';
		$post_items .= '<time datetime="' . esc_attr(get_the_time('c')) . '">';
		if ( !empty($icon) ) {
			$post_items .= '<i class="'.esc_attr($icon).'"></i> ';
		}
		$post_items .= esc_attr(get_the_time($date_format));
		$post_items .= '</time>';
		if ( !empty($disp_cat) && ( !empty($taxonomy) && !empty($term) ) ) {
			$post_items .= '<span class="' . $cat_slug .'">'. $cat_name .'</span>';
		}
		$post_items .= '<a href="'. esc_url(get_the_permalink()) .'" rel="bookmark">' . esc_attr(get_the_title()) . '</a>';
		$post_items .= '</li>';
	endwhile;

	return
		'<div class="posts-list"><ul'.$list_class_var.'>'
		. $post_items
		. '</ul></div>' .
	wp_reset_query();
}
add_shortcode( 'postlist', 'postlist_shortcode' );

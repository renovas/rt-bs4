<?php

/**
 * 機能追加
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * 関数一覧
 * add_title_tag() : titleタグの追加
 * custom_document_title_separator() : タイトルの区切り文字を変更
 * add_feed_link() : RSSフィードへのリンクを追加
 * enable_refresh_widgets() : ウィジェットで編集ショートカットを有効化
 * my_editor_styles() : ビジュアルエディター用のCSS
 * add_favicon() : ファビコンの設置
 * enable_thumbnail() : アイキャッチ画像の使用
 * set_html5_markup() : HTML5形式のマークアップを有効可
 * add_meta_format_detection() : 電話番号／メール／住所の自動リンク機能を無効化
 * add_page_current_body_class() : body_classにカレントページのスラッグを追加
 * set_admin_menu_width() : 管理画面のサイドメニューの横幅を変更
 * remove_unnecessary_meta() : ヘッダから不要な情報を削除する
 * disable_emoji() : WP4.2から追加された絵文字対応のスクリプトを無効化
 * disable_srcset() : WP4.4から追加になった画像出力機能「srcset」を無効化
 * remove_script_version() : JS/CSS読み込み時のクエリ文字列を削除
 * disable_visual_editor_in_page() : 任意の投稿タイプでビジュアルエディタを非表示にする
 * remove_admin_bar_menu() : 管理バー（Admin bar）のメニューを削除
 * add_google_analytics_field() : 管理画面の[設定]>[一般設定]の中にGoogleアナリティクスのトラッキングコード設定欄を作成
 * add_google_analytics_code : Googleアナリティクスのトラッキングコードを出力
 * add_google_site_verification_field() : 管理画面の[設定]>[一般設定]の中にGoogle Search Consoleの認証コード設定欄を作成
 * set_google_site_verification_code() : Google Search Consoleの認証コードを出力
 * register_page_columns_slug() : 固定ページ一覧にスラッグのカラムを追加
 * add_typesquare_api() : TypeSquareのAPIコードをheadに挿入する
 * Bootstrap用カスタムNavigstion Walker Classの定義
 * customize_wp_bootstrap_pagination() : Bootstrapのコンポーネントを使ったページ送り（矢印の設定）
 * wp_bootstrap_pagination() : Bootstrapのコンポーネントを使ったページ送り（wp_bootstrap_paginationの再定義）
 * get_primary_category() : Primary Categoryを取得
 * get_primary_category_link() : Primary Categoryのアーカイブリンクを取得
 * the_primary_category() : Primary Categoryを出力
 * custom_archive_title() : get_the_archive_title()とthe_archive_title()の出力内容をカスタマイズ
 * is_parent_slug() : 親ページのスラッグを判定
 * get_ancestor_ID() : 最上位ページのIDを取得
 * my_excerpt_mblength() : 抜粋の文字数を変更(require:WP Multibyte Patch)
 * my_excerpt_more() : 抜粋の文末文字を変更する
 * add_my_images_class() : メディアの挿入時に独自のクラスを追加する
 * remove_size_from_image_tag() : メディアの挿入時にwidthとheightを削除する
 * add_role_client() : 新しい権限グループ「クライアント」の作成
 * enable_post_formats : 投稿フォーマットを有効化する
 *
 */


// titleタグの追加
if ( ! function_exists( 'add_title_tag' ) ) :
function add_tag_title() {
  add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'add_tag_title' );
endif; // add_title_tag


// タイトルの区切り文字を変更 add_theme_support('title-tag')
if ( ! function_exists( 'custom_document_title_separator' ) ) :
function custom_document_title_separator( $sep ) {
  return '|';
}
add_filter( 'document_title_separator', 'custom_document_title_separator' );
endif; // custom_document_title_separator


// RSSフィードへのリンクを追加
if ( ! function_exists( 'add_feed_link' ) ) :
function add_feed_link() {
  add_theme_support( 'automatic-feed-links' );
}
add_action( 'after_setup_theme', 'add_feed_link' );
endif; // add_feed_link


// ウィジェットで編集ショートカットを有効化
if ( ! function_exists( 'enable_refresh_widgets' ) ) :
function enable_refresh_widgets() {
  add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'enable_refresh_widgets' );
endif; // enable_refresh_widgets


/**
 * ビジュアルエディター用のCSS
 *
 * @link https://developer.wordpress.org/reference/functions/add_editor_style/
 */
if ( ! function_exists( 'my_editor_styles' ) ) :
function my_editor_styles() {
  add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'my_editor_styles' );
endif; // my_editor_styles


// ファビコンの設置
if ( ! function_exists( 'add_favicon' ) ) :
function add_favicon() {
  echo '<link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" type="image/x-icon" href="'.get_stylesheet_directory_uri().'/favicon.ico'.'">'."\n";
  echo '<link rel="apple-touch-icon" sizes="152x152" href="'.get_stylesheet_directory_uri().'/apple-touch-icon.png'.'">'."\n";
}
add_action('wp_head', 'add_favicon');
endif; // add_favicon


// アイキャッチ画像の使用
if ( ! function_exists( 'enable_thumbnail' ) ) :
function enable_thumbnail() {
  add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'enable_thumbnail' );
endif; // enable_thumbnail


// HTML5形式のマークアップを有効可
if ( ! function_exists( 'set_html5_markup' ) ) :
function set_html5_markup() {
  $tags = array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption', );
  add_theme_support( 'html5', $tags );
}
add_action( 'after_setup_theme', 'set_html5_markup' );
endif; // set_html5_markup


// 電話番号／メール／住所の自動リンク機能を無効化
if ( ! function_exists( 'add_meta_format_detection' ) ) :
function add_meta_format_detection() {
  echo '<meta name="format-detection" content="telephone=no, email=no, address=no">'."\n";
}
add_action('wp_head', 'add_meta_format_detection');
endif; // add_meta_format_detection


// body_classにカレントページのスラッグを追加
if ( ! function_exists( 'add_page_current_body_class' ) ) :
function add_page_current_body_class( $classes ) {
  if ( is_singular() ) {
    $post_type = get_query_var( 'post_type' );
    if ( is_page() ) {
      $post_type = 'page';
    }
    global $post;
    // カレントページのスラッグ
    $classes[] = esc_attr( $post_type . '-' . $post->post_name );
  }
  return $classes;
}
add_filter( 'body_class', 'add_page_current_body_class' );
endif; // add_page_current_body_class


// 管理画面のサイドメニューの横幅を変更
if ( ! function_exists( 'set_admin_menu_width' ) ) :
function set_admin_menu_width() {
  echo '<style>
    #adminmenuback, #adminmenuwrap, #adminmenu, #adminmenu .wp-submenu { width: 200px; }
    #wpcontent, #wpfooter { margin-left: 200px; }
    #adminmenu .opensub .wp-submenu { left: 200px; }
    #adminmenu div.wp-menu-name { padding: 8px; }
    @only screen and (max-width: 960px) { .auto-fold #adminmenu a.menu-top { height: auto; } }
  </style>';
}
add_action('admin_head', 'set_admin_menu_width');
endif; // set_admin_menu_width


// ヘッダから不要な情報を削除する
if ( ! function_exists( 'remove_unnecessary_meta' ) ) :
function remove_unnecessary_meta() {
  remove_action( 'wp_head', 'feed_links', 2 ); // post Feed
  remove_action( 'wp_head', 'feed_links_extra', 3 ); // category feeds
  remove_action( 'wp_head', 'index_rel_link' ); // index link
  remove_action( 'wp_head', 'rsd_link' ); // EditURI
  remove_action( 'wp_head', 'wlwmanifest_link' ); // wlwmanifest
  remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' ); // prev,next
  remove_action( 'wp_head', 'wp_generator' ); // generator
  remove_action( 'wp_head', 'wp_shortlink_wp_head' ); // shortlink
  remove_action( 'wp_head', 'rel_canonical' ); // canonical
  // Embed機能
  remove_action('wp_head', 'rest_output_link_wp_head');
  remove_action('wp_head', 'wp_oembed_add_discovery_links');
  remove_action('wp_head', 'wp_oembed_add_host_js');
  remove_action('template_redirect', 'rest_output_link_header', 11 );
}
add_action( 'init', 'remove_unnecessary_meta' );
endif; // remove_unnecessary_meta


// WP4.2から追加された絵文字対応のスクリプトを無効化
if ( ! function_exists( 'disable_emoji' ) ) :
function disable_emoji() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );
endif; // disable_emoji


// WP4.4から追加になった画像出力機能「srcset」を無効化
if ( ! function_exists( 'disable_srcset' ) ) :
function disable_srcset($sources) {
  return false;
}
add_filter('wp_calculate_image_srcset', 'disable_srcset');
endif; // disable_srcset


// JS/CSS読み込み時のクエリ文字列を削除
if ( ! function_exists( 'remove_script_version' ) ) :
function remove_script_version( $src ){
  $parts = explode( '?ver', $src );
  return $parts[0];
}
add_filter( 'script_loader_src', 'remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'remove_script_version', 15, 1 );
endif; // remove_script_version


// 任意の投稿タイプでビジュアルエディタを非表示にする
if ( ! function_exists( 'disable_visual_editor_in_page' ) ) :
function disable_visual_editor_in_page(){
  $posttype = array('page');  // 非表示にする投稿タイプを指定
  global $typenow;
  if( in_array($typenow, $posttype) ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}
function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );
endif; // disable_visual_editor_in_page


// 管理バー（Admin bar）のメニューを削除
if ( ! function_exists( 'remove_admin_bar_menu' ) ) :
function remove_admin_bar_menu() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_node('wp-logo'); // WordPressロゴ
  //$wp_admin_bar->remove_node('site-name'); // サイト名
  $wp_admin_bar->remove_node('customize'); // カスタマイズ
  //$wp_admin_bar->remove_node('edit'); // ページを編集
  //$wp_admin_bar->remove_node('updates'); // アップデート通知
  $wp_admin_bar->remove_node('comments'); // コメント
  //$wp_admin_bar->remove_node('new-content'); // 新規追加
  //$wp_admin_bar->remove_node('view'); // 投稿を表示
  //$wp_admin_bar->remove_node('my-account');
}
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 99 );
endif; // remove_admin_bar_menu


// 管理画面の[設定]>[一般設定]の中にGoogleアナリティクスのトラッキングコード設定欄を作成
if ( ! function_exists( 'add_google_analytics_field' ) ) :
function add_google_analytics_field() {
  register_setting('general', 'ga_id', 'esc_attr');
  add_settings_field('ga_id', '<label for="ga_id">Googleアナリティクス</label>' , 'ga_id_html', 'general');
}
function ga_id_html() {
  $value = get_option( 'ga_id', '' );
  echo '<input type="text" id="ga_id" name="ga_id" value="' . $value . '" placeholder="UA-XXXXXXX-X">';
}
add_filter('admin_init', 'add_google_analytics_field');
endif; // add_google_analytics_field


// Googleアナリティクスのトラッキングコードを出力
if ( ! function_exists( 'add_google_analytics_code' ) ) :
function add_google_analytics_code() {
  if ( get_option('ga_id') ) {
    echo '<script async src="https://www.googletagmanager.com/gtag/js?id=' . get_option('ga_id') . '"></script>'."\n";
    echo '<script>'."\n";
    echo 'window.dataLayer = window.dataLayer || [];'."\n";
    echo 'function gtag(){dataLayer.push(arguments);}'."\n";
    echo 'gtag("js", new Date());'."\n";
    echo 'gtag("config", "' . get_option('ga_id') . '");' . "\n";
    echo '</script>'."\n";
  }
}
add_action('wp_head', 'add_google_analytics_code', 0);
endif; // add_google_analytics_code


// 管理画面の[設定]>[一般設定]の中にGoogle Search Consoleの認証コード設定欄を作成
if ( ! function_exists( 'add_google_site_verification_field' ) ) :
function add_google_site_verification_field() {
  register_setting('general', 'google_site_verification', 'esc_attr');
  add_settings_field('google_site_verification', '<label for="google_site_verification">Search Console 認証コード</label>' , 'searchconsole_html', 'general');
}
function searchconsole_html() {
  $value = get_option( 'google_site_verification', '' );
  echo '<input type="text" id="google_site_verification" name="google_site_verification" value="' . $value . '">';
}
add_filter('admin_init', 'add_google_site_verification_field');
endif; // add_google_site_verification_field


// Google Search Consoleの認証コードを出力
if ( ! function_exists( 'set_google_site_verification_code' ) ) :
function set_google_site_verification_code() {
  if ( get_option('google_site_verification') ) {
    echo '<meta name="google-site-verification" content="' . get_option('google_site_verification') . '" />'."\n";
  }
}
add_action('wp_head', 'set_google_site_verification_code');
endif; // set_google_site_verification_code


// 固定ページ一覧にスラッグのカラムを追加
if ( ! function_exists( 'register_page_columns_slug' ) ) :
function register_page_columns_slug( $columns ) {
  $columns['slug'] = "スラッグ";
  return $columns;
}
function add_pages_column_slug( $column_name, $post_id ) {
  if( $column_name == 'slug' ) {
    $post = get_post($post_id);
    $slug = $post->post_name;
    echo esc_attr($slug);
  }
}
add_filter( 'manage_edit-page_sortable_columns', 'register_page_columns_slug' );
add_filter( 'manage_pages_columns', 'register_page_columns_slug');
add_action( 'manage_pages_custom_column', 'add_pages_column_slug', 10, 2);
endif; // register_page_columns_slug


// TypeSquareのAPIコードをheadに挿入する
if ( ! function_exists( 'add_typesquare_api' ) ) :
function add_typesquare_api() {
  echo '<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?WzeGoQqM0A0%3D" charset="utf-8"></script>'."\n";
}
add_action('wp_head', 'add_typesquare_api');
endif; // add_typesquare_api


// Bootstrap用カスタムNavigstion Walker Classの定義
require_once( get_template_directory().'/inc/class-wp-bootstrap-navwalker.php' );


// Bootstrapのコンポーネントを使ったページ送り
// 矢印の設定
if ( ! function_exists( 'customize_wp_bootstrap_pagination' ) ) :
function customize_wp_bootstrap_pagination($args) {
  $args['previous_string'] = '<i class="fa fa-angle-left fa-fw"></i>';
  $args['next_string'] = '<i class="fa fa-angle-right fa-fw"></i>';
  $args['first_string'] = '<i class="fa fa-angle-double-left fa-fw"></i>';
  $args['last_string'] = '<i class="fa fa-angle-double-right fa-fw"></i>';
  return $args;
}
add_filter('wp_bootstrap_pagination_defaults', 'customize_wp_bootstrap_pagination');
endif; // customize_wp_bootstrap_pagination


// wp_bootstrap_pagination()の再定義
if ( ! function_exists( 'wp_bootstrap_pagination' ) ) :
function wp_bootstrap_pagination( $args = array() ) {
  // デフォルト設定
    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => 'Previous',
        'next_string'     => 'Next',
        'first_string'    => 'First',
        'last_string'     => 'Last',
        'before_output'   => '<nav class="pagination-wrap text-center"><ul class="pagination justify-content-center">',
        'after_output'    => '</ul></nav>'
    );

    $args = wp_parse_args(
        $args,
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );

    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );

    if ( $count <= 1 )
        return FALSE;

    if ( !$page )
        $page = 1;

    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }

    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );

    $firstpage = esc_attr( get_pagenum_link(1) );
    if ( $firstpage && (1 != $page) )
        $echo .= '<li class="page-item"><a class="page-link" href="' . $firstpage . '" title="' . 'first' . '">' . $args['first_string'] . '</a></li>';

    if ( $previous && (1 != $page) )
        $echo .= '<li class="page-item"><a class="page-link" href="' . $previous . '" title="' . 'previous' . '">' . $args['previous_string'] . '</a></li>';

    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="page-item active"><a class="page-link" href="#">' . str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) . '</a></li>';
            } else {
                $echo .= sprintf( '<li class="page-item"><a class="page-link" href="%s">%002d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }

    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li class="page-item"><a class="page-link" href="' . $next . '" title="' . 'next' . '">' . $args['next_string'] . '</a></li>';

    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( $lastpage ) {
        $echo .= '<li class="page-item"><a class="page-link" href="' . $lastpage . '" title="' . 'last' . '">' . $args['last_string'] . '</a></li>';
    }

    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}
endif; // wp_bootstrap_pagination


// Yoast SEOのPrimary Categoryを出力する
// get_primary_category() : Primary Categoryを取得
// get_primary_category_link() : Primary Categoryのアーカイブリンクを取得
// the_primary_category() : Primary Categoryを出力
if ( ! function_exists( 'get_primary_category' ) ) :
function get_primary_category( $post = 0 ) {
  $post = get_post( $post );
  $id = isset( $post->ID ) ? $post->ID : 0;

  // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
  $category = get_the_category($id);
  // If post has a category assigned.
  if ($category){
    $category_display = '';
    if ( class_exists('WPSEO_Primary_Term') ) {
      // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term( $wpseo_primary_term );
      if (is_wp_error($term)) {
        // Default to first category (not Yoast) if an error is returned
        $category_display = $category[0]->name;
      } else {
        // Yoast Primary category
        $category_display = $term->name;
      }
    } else {
      // Default, display the first category in WP's list of assigned categories
      $category_display = $category[0]->name;
    }
    return htmlspecialchars($category_display);
  }
}
endif; // get_primary_category


if ( ! function_exists( 'get_primary_category_link' ) ) :
function get_primary_category_link( $post = 0 ) {
  $post = get_post( $post );
  $id = isset( $post->ID ) ? $post->ID : 0;

  // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
  $category = get_the_category($id);
  // If post has a category assigned.
  if ($category){
    $category_link = '';
    if ( class_exists('WPSEO_Primary_Term') ) {
      // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term( $wpseo_primary_term );
      if (is_wp_error($term)) {
        // Default to first category (not Yoast) if an error is returned
        $category_link = get_category_link( $category[0]->term_id );
      } else {
        // Yoast Primary category
        $category_link = get_category_link( $term->term_id );
      }
    } else {
      // Default, display the first category in WP's list of assigned categories
      $category_link = get_category_link( $category[0]->term_id );
    }
    return $category_link;
  }
}
endif; // get_primary_category_link


if ( ! function_exists( 'the_primary_category' ) ) :
function the_primary_category( $before = '', $after = '', $echo = true ) {
  $category = get_primary_category();

  if ( strlen($category) == 0 ) {
    return;
  }

  $category = $before . $category . $after;

  if ( $echo ) {
    echo $category;
  } else {
    return $category;
  }
}
endif; // the_primary_category


// get_the_archive_title()とthe_archive_title()の出力内容をカスタマイズ
if ( ! function_exists( 'custom_archive_title' ) ) :
function custom_archive_title( $title ){
  if ( is_tax() ) {
    $title = single_term_title( '', false );
  } elseif ( is_post_type_archive() ) {
    $title = post_type_archive_title( '', false );
  }

  return $title;
}
add_filter( 'get_the_archive_title', 'custom_archive_title', 10 );
endif; // custom_archive_title


// 親ページのスラッグを判定
if ( ! function_exists( 'is_parent_slug' ) ) :
function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
  return $post_data->post_name;
  }
}
endif; // is_parent_slug


// 最上位ページのIDを取得
if ( ! function_exists( 'get_ancestor_ID' ) ) :
function get_ancestor_ID() {
  global $post;
  if ( is_singular() ) {
    $post_type = get_query_var('post_type') ? get_query_var('post_type') : $post->post_type;
    $post_id = get_query_var('p') ? get_query_var('p') : $post->ID;
    if ( is_page() ) {
      $post_type = 'page';
    }
    if ( $post_type && is_post_type_hierarchical($post_type) ) {
      if ( $post->ancestors ) {
        $post_id = $post->ancestors[count($post->ancestors) - 1];
      } else {
        $post_id = esc_attr($post->ID);
      }
    }
  } else {
    $post_id = '';
  }
  return $post_id;
}
endif; // get_ancestor_ID


// 抜粋の文字数を変更(require:WP Multibyte Patch)
if ( ! function_exists( 'my_excerpt_mblength' ) ) :
function my_excerpt_mblength($length) {
  return 90;
}
add_filter('excerpt_mblength', 'my_excerpt_mblength');
endif; // my_excerpt_mblength


// 抜粋の文末文字を変更する
if ( ! function_exists( 'my_excerpt_more' ) ) :
function my_excerpt_more($more) {
  return ' ...';
}
add_filter('excerpt_more', 'my_excerpt_more');
endif; // my_excerpt_more


// メディアの挿入時に独自のクラスを追加する
if ( ! function_exists( 'add_my_images_class' ) ) :
function add_my_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
  $classes = 'img-fluid';
  if ( preg_match('/<img.*? class=".*?" \/>/', $html) ) {
    $html = preg_replace('/(<img.*? class=".*?)(".*?\/>)/', '$1 ' . $classes . '$2', $html);
  } else {
    $html = preg_replace('/(<img.*?)\/>/', '$1 class="' . $classes . '" >', $html);
  }
  return $html;
}
add_filter( 'image_send_to_editor', 'add_my_images_class', 10, 8 );
endif; // add_my_images_class


// メディアの挿入時にwidthとheightを削除する
if ( ! function_exists( 'remove_size_from_image_tag' ) ) :
function remove_size_from_image_tag( $html, $id, $alt, $title, $align, $size ) {
  list( $img_src, $width, $height ) = image_downsize($id, $size);
  $remove_size_from_img = image_hwstring( $width, $height );
  $html = str_replace( $remove_size_from_img, '', $html );
  return $html;
}
add_filter( 'get_image_tag', 'remove_size_from_image_tag', 10, 6 );
endif; // remove_size_from_image_tag


// 新しい権限グループ「クライアント」の作成
if ( ! function_exists( 'add_role_client' ) ) :
function add_role_client() {
  add_role( 'client', 'クライアント',
    array(
      'read'                    => true,
      'delete_posts'            => true,
      'edit_posts'              => true,
      'delete_published_posts'  => true,
      'publish_posts'           => true,
      'upload_files'            => true,
      'edit_published_posts'    => true,
      'unfiltered_html'         => true,
      'read_private_posts'      => true,
      'edit_private_posts'      => true,
      'delete_private_posts'    => true,
      'delete_others_posts'     => true,
      'edit_others_posts'       => true,
      'manage_categories'       => true,
      'moderate_comments'       => false,
      'edit_comment'            => false,
    )
  );
}
add_action( 'after_switch_theme', 'add_role_client' );


// 左サイドメニューから項目を削除
function remove_admin_menu_client() {
  if ( !current_user_can('client') ) return false;
  remove_menu_page('edit-comments.php'); // コメント
  remove_menu_page('tools.php'); // ツール
}
add_action( 'admin_menu', 'remove_admin_menu_client' );

// ダッシュボードの項目を削除
function remove_dashboard_widget_client() {
  if ( !current_user_can('client') ) return false;
  global $wp_meta_boxes;
  //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // アクティビティ
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況（概要）
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
  unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']); // Yoast SEO プラグイン
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widget_client' );
endif; // add_role_client


// 投稿フォーマットを有効化する
if ( ! function_exists( 'enable_post_formats' ) ) :
function enable_post_formats() {
  //add_post_type_support( array('post'), 'post-formats' );
  add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'link', 'quote', 'status', 'video', 'audio', 'chat' ) );
}
add_action( 'after_setup_theme', 'enable_post_formats' );
endif; // enable_post_formats

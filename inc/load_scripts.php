<?php

/**
 * JSとCSSの読み込み
 * @link https://codex.wordpress.org/Function_Reference/wp_enqueue_script
 * @link https://codex.wordpress.org/Function_Reference/wp_enqueue_style
 *
 * 関数一覧
 * load_jquery_for_bs4 : BS4用のjQueryの読み込み
 * load_Bootstrap : Bootstrapの読み込み
 * load_fontawesome : Font Awesomeの読み込み
 * defer_parsing_js : 読み込むjsにdefer属性を付与
 * load_fonts : ウェブフォントの読み込み
 * load_slider : スライダ／カルーセル用ライブラリの読み込み
 * load_library : その他ライブラリの読み込み
 * load_my_script : 独自JSの読み込み
 * load_my_style : 独自CSSの読み込み
 *
 */


// BS4用のjQueryの読み込み
if ( !function_exists('load_jquery_for_bs4') ) :
function load_jquery_for_bs4() {
  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    //wp_enqueue_script('jquery','https://code.jquery.com/jquery-3.3.1.slim.min.js', array(), '3.3.1', false );
    wp_enqueue_script('jquery','https://code.jquery.com/jquery-3.3.1.min.js', array(), '3.3.1', false );
  }
}
add_action( 'wp_enqueue_scripts', 'load_jquery_for_bs4' );
endif; // load_jquery_for_bs4


// Bootstrapの読み込み
if ( !function_exists('load_bootstrap') ) :
function load_bootstrap() {
  // Bootstrap https://getbootstrap.com/
  //wp_enqueue_style( 'bootstrap_style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css', array(), '4.1.0' );
  //wp_enqueue_script( 'bootstrap_script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js', array('jquery','popper'), '4.1.0', true );
  //wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js', array('jquery'), '1.14.0', true );

  // カスタマイズ版Bootstrap
  wp_enqueue_style( 'bootstrap_style', get_template_directory_uri() . '/assets/css/rtbs4.bootstrap.css', array(), '4.1.0' );
  wp_enqueue_script( 'bootstrap_script', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.bundle.min.js', array('jquery'), '4.1.0', true );
}
add_action( 'wp_enqueue_scripts', 'load_bootstrap' );
endif; // load_bootstrap


// Font Awesomeの読み込み
if ( !function_exists('load_fontawesome') ) :
function load_fontawesome() {
  // Font Awesome http://fontawesome.io/ https://fontawesome.com/
  //wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array('bootstrap_style'), '4.7.0' );
  wp_enqueue_script( 'fontawesome', get_template_directory_uri() . '/assets/fontawesome/fontawesome-all.min.js', array('jquery'), '5.0.10', true );
}
add_action( 'wp_enqueue_scripts', 'load_fontawesome' );
endif; // load_fontawesome


// 読み込むjsにdefer属性を付与
if ( !function_exists('add_defer_script') ) :
function add_defer_script ( $tag, $handle ) {
  // Font Awesomeだけを対象にする
  if ( 'fontawesome' != $handle || is_admin() ) return $tag;
  // defer属性の付与
  return str_replace( ' src', ' defer src', $tag );
}
add_filter( 'script_loader_tag', 'add_defer_script', 10, 2 );
endif; // add_defer_script


// ウェブフォントの読み込み
if ( !function_exists('load_fonts') ) :
function load_fonts() {
  $fonts_google_args = array(
    'family' => '',
    'subset' => '',
  );
  wp_enqueue_style( 'fonts_google', add_query_arg( $fonts_google_args, 'https://fonts.googleapis.com/css' ), array(), '' );
}
add_action( 'wp_enqueue_scripts', 'load_fonts' );
endif; // load_fonts


// その他ライブラリの読み込み
if ( !function_exists('load_library') ) :
function load_library() {
  // jquery.easing http://gsgd.co.uk/sandbox/jquery/easing/
  wp_enqueue_script( 'jquery_easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'load_library' );
endif; // load_library


// 独自JSの読み込み
if ( !function_exists('load_my_script') ) :
function load_my_script() {
  wp_enqueue_script( 'my_script', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery','bootstrap_script'), '', true );
}
add_action( 'wp_enqueue_scripts', 'load_my_script' );
endif; // load_my_script


// 独自CSSの読み込み
if ( !function_exists('load_my_style') ) :
function load_my_style() {
  //wp_enqueue_style( 'my_style', get_stylesheet_uri(), array('bootstrap_css','fontawesome','fonts_google'), '' );
  wp_enqueue_style( 'my_style', get_template_directory_uri() . '/style.css', array('bootstrap_style','fontawesome','fonts_google'), '' );
}
add_action( 'wp_enqueue_scripts', 'load_my_style' );
endif; // load_my_style

<footer id="colophon" class="site-footer">
  <?php if ( has_nav_menu( 'footer-navi' ) ) : ?>
    <?php wp_nav_menu( array(
      'theme_location'  => 'footer',
      'container'       => 'nav',
      'container_class' => 'footer-navigation',
      'container_id'    => '',
      'menu_class'      => '',
      'link_before'     => '<i class="fa fa-caret-right" aria-hidden="true"></i> ',
      'link_after'      => '',
    ) ); ?>
  <?php endif; ?>
</footer><!-- #colophon -->



</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
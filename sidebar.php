<?php if ( !is_active_sidebar('sidebar-widget') || is_front_page() ) { return; } ?>

<aside id="secondary" class="widget-area col-lg-3">
  <div class="sidebar-widget-wrap">
    <?php dynamic_sidebar('sidebar-widget'); ?>
  </div><!-- / .sidebar-widget-wrap -->

  <?php if ( is_active_sidebar('sidebar-banner') ) : ?>
  <div class="sidebar-banner-wrap">
    <?php dynamic_sidebar('sidebar-banner'); ?>
  </div><!-- / .sidebar-banner-wrap -->
  <?php endif; ?>
</aside><!-- #secondary -->
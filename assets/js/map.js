var map;
function initialize() {
	// マーカーの座標
	var makerLatLng = '35.14201, 139.1010829';
	// 表示中央の座標
	var centerLatLng = '35.144, 139.107';

	// カラー設定
	var stylePref = 'gunma';
	var styleLandscape = '#FFBB00';
	var styleRoadHighway = '#FFC200';
	var styleRoadArterial = '#FF0300';
	var styleRoadLocal = '#FF0300';
	var styleWater = '#0078FF';
	var styleMarkerFill = '#75ADDC';
	var styleMarkerStroke = '#FFFFFF';

	// オプション設定
	var mapOptions = {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: false,
		zoom: 17,
		zoomControl: false,
		panControl: false,
		streetViewControl: false,
		scaleControl: false,
		overviewMapControl: false,
		scrollwheel: false,
		draggable: false,
		disableDoubleClickZoom: true,
		keyboardShortcuts: false,
		center: new google.maps.LatLng(centerLatLng)
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	var mapStyles = [

{"featureType":"landscape","stylers":[{"hue":styleLandscape},{"saturation":43.400000000000006},{stylePref:37.599999999999994},{stylePref:1}]},
{"featureType":"road.highway","stylers":[{"hue":styleRoadHighway},{"saturation":-61.8},{"lightness":45.599999999999994},{stylePref:1}]},
{"featureType":"road.arterial","stylers":[{"hue":styleRoadArterial},{"saturation":-100},{"lightness":51.19999999999999},{stylePref:1}]},
{"featureType":"road.local","stylers":[{"hue":styleRoadLocal},{"saturation":-100},{"lightness":52},{stylePref:1}]},
{"featureType":"poi","stylers":[{"visibility":"simplified"}]},
{"featureType":"water","stylers":[{"hue":styleWater},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{stylePref:1}]},

	];
	map.setOptions({styles: mapStyles});

	var infoContent = '<div class="window-content"><h4>社名</h4><p>所在地</p></div>';

	var infowindow = new google.maps.InfoWindow({
		content: infoContent
	});

	var icon = {
		path: 'M0,38.9947301 C0,40.1022101 0.887729645,41 2,41 L2,41 C3.1045695,41 4,40.1055269 4,38.9947301 L4,2.00526988 C4,0.897789907 3.11227036,0 2,0 L2,0 C0.8954305,0 0,0.894473121 0,2.00526988 L0,38.9947301 L0,38.9947301 Z M5,2 L5,20 L25,20 L18,11 L25,2 L5,2 L5,2 Z',
		anchor: new google.maps.Point(2, 41),
		fillColor: styleMarkerFill,
		fillOpacity: 1,
		strokeWeight: 1,
		strokeColor: styleMarkerStroke,
		strokeOpacity: 1,
		scale: 1
	};

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(makerLatLng),
		map: map,
		icon: icon,
		title: 'marker'
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});

}
google.maps.event.addDomListener(window, 'load', initialize);

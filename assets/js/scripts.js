// HTMML(DOM)の構築完了後に実行 = jQuery(document).ready(function($) {
( function( $ ) {




	/* ------------------------------
	* JSのパスを取得
	------------------------------ */
	var self = (function() { /* JS自身のパスを取得 */
		if (document.currentScript) {
			return document.currentScript.src;
		} else {
			var scripts = document.getElementsByTagName('script'),
			script = scripts[scripts.length-1];
			if (script.src) {
				return script.src;
			}
		}
	})();
	var path = self.match(/^h.+wp\-content/); /* wp-contentまでのパスを抽出 */
	var theme = 'rt-bs4';
	/* END OF JS PATH ============================== */




	/* ------------------------------
	* スライダ系ライブラリ
	------------------------------ */
	// slick http://kenwheeler.github.io/slick/
	if (typeof $.slick == 'function') {
		$('.slick').slick({
			dots: true,
			infinite: true,
			speed: 500,
			//fade: true,
			//cssEase: 'linear',
			autoplay: true,
			autoplaySpeed: 2000,
		});
	}

	// FlexSlider https://woocommerce.com/flexslider/
	if (typeof $.flexslider == 'function') {
		$('.flexslider').flexslider();
	}

	// Owl Carousel https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html
	if (typeof $.owlCarousel == 'function') {
		$('.owl-carousel').owlCarousel();
	}

	// bxSlider http://bxslider.com/options
	if (typeof $.bxSlider == 'function') {
		$('.bxslider').bxSlider();
	}

	// Glide.js http://glide.jedrzejchalubek.com/docs.html#options
	if (typeof $.glide == 'function') {
		$('.glide').glide({ type:'carousel' });
	}
	/* END OF SLIDER JS SNIPPETS ============================== */




	/* ------------------------------
	* ライトボックス系ライブラリ
	------------------------------ */
	// Fluidbox http://mdeboer.github.io/jquery-fluidbox/#options
	if (typeof $.fluidbox == 'function') {
		//$('a[rel="lightbox"]').fluidbox();
		$("a").filter(function(){ return this.href.match(/\.(jpg|jpeg|png|gif)$/i); }).fluidbox();
	}

	// FancyBox 1.3.4 http://fancybox.net/
	if (typeof $.fancybox == 'function') {
		$("a").filter(function(){ return this.href.match(/\.(jpg|jpeg|png|gif)$/i); }).fancybox();
	}
	/* END OF LIGHTBOX JS SNIPPETS ============================== */




	/* ------------------------------
	* その他
	------------------------------ */
	// カーニング
	if (typeof $.kerning == 'function') {
		$.getJSON(path　+ '/themes/' + theme + '/js/kerning-data.json' , function(_data) {
			$('.kerning').kerning({"data":_data});
		});
	}


	// スムーズスクロール
	$('a[href^="#"]').not('a[data-toggle="collapse"]').on('click', function(){
		var speed = 1000;
		var href= $(this).attr('href');
		var target = $(href == '#' || href == '' ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, speed, 'easeInOutCirc');
		return false;
	});


	// ページの先頭へ戻るボタン
	var pagetop = $('.pagetop');
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			pagetop.fadeIn();
		} else {
			pagetop.fadeOut();
		}
	});
	pagetop.click(function () {
		$('#page').animate({ scrollTop: 0 }, 1200, 'easeInOutQuart');
		return false;
	});


	// ロールオーバー（フェード）
	$("a.rollover img, #secondary a img, .entry-content a img").hover(
		function(){ $(this).stop().animate({"opacity":0.65}); },
		function(){ $(this).stop().animate({"opacity":1}); }
	);


	// PDFファイルを別タブで開く
	$(".entry-content a[href$='.pdf']").attr("target","_blank");


	// 外部リンクを別タブで開く
	$("a[href^='http'], a[href^='//']").each(function() {
		if(this.href.indexOf(location.hostname) == -1) {
			$(this).attr({ target: "_blank" });
		} else {
			$(this).removeAttr("target");
		}
	});


	// 郵便番号->住所
	// memo: オリジナルのjquery.zip2addr.jsにちょっとした加工が必要→「;(function($) { ~~~~~ })(jQuery);」で括る
	if (typeof $.zip2addr == 'function') {
		if ( document.URL.match("/contact/") ) {
			$("input[name='郵便番号[data][0]']").zip2addr({
					zip2:"input[name='郵便番号[data][1]']",
					pref:"select[name='都道府県']",
					addr:"input[name='市区町村など']"
			});
		}
	}


	// Bootstrap
	$('[data-toggle="popover"]').popover({ trigger: 'focus' });
	$('[data-toggle="tooltip"]').tooltip();




	/* ------------------------------
	* 画像などすべてのページ要素のロード後と画面サイズの変更時に実行
	------------------------------ */
	$(window).on('load resize',function(){

		// ウィンドウサイズによって処理を変える
		var current_width = jQuery(window).width();
		if ( current_width > 768 ) {
			// 769px以上の場合

		} else if ( current_width > 554 ) {
			// 555px以上の場合

		} else {
			// 554px以下の場合

		}

	});
	/* END OF PAGE LOAD AND WINDOW RESIZE ============================== */




	/* ------------------------------
	* 画像などすべてのページ要素のロード後に実行
	------------------------------ */
	$(window).on('load',function(){

		// jquery.matchHeight.js https://github.com/liabru/jquery-match-height
		if (typeof $.zip2addr == 'function') {
			$('.item').matchHeight({
				byRow: true,
				property: 'height',
				target: null,
				remove: false
			});
		}

	});
	/* END OF PAGE LOAD ============================== */




} )( jQuery );

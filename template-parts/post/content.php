<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title h1">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta"><div class="row">
			<div class="metadata col-lg-6">
				<time datetime="<?php the_time('c'); ?>"><?php the_time('Y.m.d'); ?></time>
				<span class="post-category"><?php echo get_primary_category($post->ID); ?></span>
			</div><!-- / .metadata -->

			<nav class="share col-lg-6 text-center text-lg-right">
				<?php get_template_part( 'template-parts/module', 'share' ); ?>
			</nav><!-- / .share -->
		</div></div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->


	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->


	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

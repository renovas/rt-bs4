	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title h1">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta"><div class="row">
			<div class="metadata col-lg-6">
				<time datetime="<?php the_time('c'); ?>" pubdate="pubdate"><?php the_time('Y.m.d'); ?></time>
				<span class="post-category"><?php echo get_primary_category($relation->ID); ?></span>
			</div><!-- / .metadata -->

			<nav class="share col-lg-6 text-center text-lg-right">
				<ul class="nav nav-inline">
					<li class="nav-item share__facebook"><a class="nav-link" href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>" onclick="window.open(encodeURI(decodeURI(this.href)), 'FBwindow', 'width=670, height=340, menubar=no, toolbar=no, scrollbars=yes'); return false;" rel="nofollow">
						<i class="fa fa-facebook-square fa-lg"></i>
						</a></li>

					<li class="nav-item share__twitter"><a class="nav-link" href="https://twitter.com/intent/tweet?text=<?php $str = get_the_title().' - '.get_bloginfo('name'); echo mb_convert_encoding($str, 'UTF-8'); ?>&url=<?php the_permalink(); ?>" target="_blank">
						<i class="fa fa-twitter-square fa-lg"></i>
					</a></li>

					<li class="nav-item share__email"><a class="nav-link" href="mailto:?subject=<?php $str_subject = '['.get_bloginfo('name').'] ページURLをメールで送信'; echo mb_convert_encoding($str_subject, 'UTF-8'); ?>&body=<?php $str_body = 'ページURLをメールで送信します。'.'%0A%0A'.get_the_title().' - '.get_bloginfo('name').'%0A'.get_the_permalink(); echo mb_convert_encoding($str_body, 'UTF-8'); ?>">
						<i class="fa fa-envelope-square fa-lg"></i>
					</a></li>
				</ul>
			</nav><!-- / .share -->
		</div></div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->


	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->


	<footer class="entry-footer">
	</footer><!-- .entry-footer -->

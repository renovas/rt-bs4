<?php if ( have_posts() ) : ?>
<div class="posts-list"><ul>
<?php while ( have_posts() ) : the_post(); ?>
<li id="post-<?php the_ID(); ?>" <?php post_class('post-list-item'); ?>><time datetime="<?php the_time('c'); ?>"><?php the_time('Y年m月d日'); ?></time><a href="<?php the_permalink(); ?>" rel="bookmark" class="fa-sup before-caret-right"><?php the_title(); ?></a></li>
<?php endwhile; ?>
</ul></div>
<?php wp_bootstrap_pagination(); ?>
<?php else : endif; ?>

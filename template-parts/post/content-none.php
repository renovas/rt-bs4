<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title h1">
			<?php if ( is_search() ) : ?>
				<div class="search__header__keyword">検索キーワード: <?php echo get_search_query() ?></div>
			<?php else : ?>
				お探しのページは見つかりませんでした
			<?php endif; ?>
		</h1>
	</header><!-- .page-header -->

	<div class="page-content">

		<?php if ( is_search() ) : ?>
		<p>ご指定の検索条件に合うページがありませんでした。誠にお手数ですが、他のキーワードで再度検索いただくか、下記のサイトマップよりコンテンツをお探しください。</p>
		<?php else : ?>
		<p>ご指定のコンテンツを見つけられませんでした。誠にお手数ですが、下記のサイトマップよりコンテンツをお探しください。</p>
		<?php endif; ?>

		<p><a href="<?php echo home_url('/'); ?>" class="btn btn-primary">トップページに戻る</a></p>
		<p><a href="<?php echo site_url('/sitemap/'); ?>" class="btn btn-primary">サイトマップを見る</a></p>

	</div><!-- .page-content -->
</section><!-- .no-results -->

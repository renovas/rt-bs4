<ul class="post__list">
<?php while ( have_posts() ) : the_post(); ?>

<li class="row post__list__item">
<div class="col-lg-3 image">
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('small','class=img-fluid img-bordered'); ?></a>
</div>
<div class="col-lg-9 text">
	<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<time class="time" datetime="<?php the_time('c'); ?>" pubdate="pubdate"><?php the_time('Y.m.d'); ?></time>
	<a class="post-category" href="<?php echo get_primary_category_link(); ?>"><?php echo get_primary_category(); ?></a>
	<div class="desc"><?php
		// echo wp_trim_words( get_the_content(), 80, '...' );
		// echo wp_trim_words( get_the_excerpt(), 80, '...' );

		$content = get_the_content();
		preg_match_all("/\[.+?\]/",$content,$retArr);
		echo wp_html_excerpt( str_replace($retArr[0],'',$content), 126, '...' );
	?></div>
</div>
</li><!-- / .post__list__item -->

<?php endwhile; ?>
</ul><!-- / .post__list -->

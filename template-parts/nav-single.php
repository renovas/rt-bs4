<nav class="pager-wrap">
  <ul class="pager">
    <?php
    $next = get_next_posts_link();
    $prev = get_previous_posts_link();
    $terms = get_the_terms($post->ID, 'post_group');
    if ( is_object_in_term($post->ID, 'post_group') ) {
      $post_term = array_shift($terms);
      $archive_link = get_term_link($post_term->slug, 'post_group');
      $archive_label = $post_term->name.'の記事';
    } elseif (!is_singular('post')) {
      $post_type = get_post_type();
      $post_type_obj = get_post_type_object($post_type);
      $archive_link = get_post_type_archive_link($post_type_obj->name);
      $archive_label = $post_type_obj->label;
    } else {
      $page_obj = get_page_by_path('all');
      $archive_link = get_page_link($page_obj->ID);
      $archive_label = 'の記事';
    }
    $disabled = empty($prev) || empty($next) ? ' disabled' : '';
    ?>
    <?php $prev = '<i class="fa fa-chevron-circle-left"></i> 前の記事'; if ( get_previous_post_link() ) { echo '<li class="pager-prev">'.get_previous_post_link('%link', $prev).'</li>'; } else { echo '<li class="pager-prev disabled"><a href="">'.$prev.'</a></li>'; } ?>
    <li><a href="<?php echo $archive_link; ?>"><?php echo $archive_label; ?>一覧</a></li>
    <?php $next = '次の記事 <i class="fa fa-chevron-circle-right"></i>'; if ( get_next_post_link() ) { echo '<li class="pager-next">'.get_next_post_link('%link', $next).'</li>'; } else { echo '<li class="pager-next disabled"><a href="">'.$next.'</a></li>'; } ?>
  </ul><!-- / .pager -->
</nav><!-- / .pager-wrap -->

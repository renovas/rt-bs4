<ul class="nav nav-inline share fa-2x">
  <li class="nav-item share-facebook">
    <a class="nav-link" href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>" onclick="window.open(encodeURI(decodeURI(this.href)), 'FBwindow', 'width=670, height=340, menubar=no, toolbar=no, scrollbars=yes'); return false;" rel="nofollow">
      <span class="fa-layers fa-fw">
        <i class="fas fa-circle"></i>
        <i class="fa-inverse fab fa-facebook-f" data-fa-transform="shrink-6"></i>
      </span><!-- / .fa-layers -->
    </a><!-- / .nav-link -->
  </li><!-- / .share-facebook -->

  <li class="nav-item share-twitter">
    <a class="nav-link" href="https://twitter.com/intent/tweet?text=<?php $str = get_the_title().' - '.get_bloginfo('name'); echo mb_convert_encoding($str, 'UTF-8'); ?>&url=<?php the_permalink(); ?>" target="_blank">
      <span class="fa-layers fa-fw">
        <i class="fas fa-circle"></i>
        <i class="fa-inverse fab fa-twitter" data-fa-transform="shrink-6"></i>
      </span><!-- / .fa-layers -->
    </a><!-- / .nav-link -->
  </li><!-- / .share-twitter -->

  <li class="nav-item share-email">
    <a class="nav-link" href="mailto:?subject=<?php $str_subject = '['.get_bloginfo('name').'] ページURLをメールで送信'; echo mb_convert_encoding($str_subject, 'UTF-8'); ?>&body=<?php $str_body = 'ページURLをメールで送信します。'.'%0A%0A'.get_the_title().' - '.get_bloginfo('name').'%0A'.get_the_permalink(); echo mb_convert_encoding($str_body, 'UTF-8'); ?>">
      <span class="fa-layers fa-fw">
        <i class="fas fa-circle"></i>
        <i class="fa-inverse fas fa-envelope" data-fa-transform="shrink-6"></i>
      </span><!-- / .fa-layers -->
    </a><!-- / .nav-link -->
  </li><!-- / .share-email -->
</ul><!-- / .share -->

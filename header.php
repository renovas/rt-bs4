<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">



<header id="masthead" class="site-header">
  <div class="site-navigation">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">
        SITE LOGO TEST
      </a><!-- / .navbar-brand -->

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button><!-- / .navbar-toggler -->

      <?php wp_nav_menu( array(
        'theme_location'  => 'global',
        'depth'           => 1, // 1 = with dropdowns, 0 = no dropdowns.
        'container'       => 'div',
        'container_class' => 'collapse navbar-collapse',
        'container_id'    => 'navbarToggler',
        'menu_class'      => 'navbar-nav mr-auto mt-2 mt-lg-0',
        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
        'walker'          => new WP_Bootstrap_Navwalker()
      ) ); ?>
    </nav><!-- / .navbar -->
  </div><!-- .site-navigation -->
</header><!-- / .site-header -->
